import React, { useEffect } from 'react'
import axios from 'axios'

export default function useBookSearch(query, pageNumber) {
    const [books, setBooks] = React.useState([])
    const [hasMore, setHasMore] = React.useState(false)
    const [loading, setLoading] = React.useState(true)
    const [error, setError] = React.useState(false)

    useEffect(() => {
        setBooks([])
    }, [query])

    useEffect(() => {
        let cancel
        setLoading(true)
        axios({
            method: 'GET',
            url: 'https://openlibrary.org/search.json',
            params: { q: query, page: pageNumber },
            cancelToken: new axios.CancelToken((c) => {
                cancel = c
            }),
        })
            .then((res) => {
                console.log(res.data)
                setBooks((prevBooks) => {
                    return [
                        ...new Set([
                            ...prevBooks,
                            ...res.data.docs.map((book) => book.title),
                        ]),
                    ]
                })
                setHasMore(res.data.docs.length > 0)
                setLoading(false)
            })
            .catch((e) => {
                if (axios.isCancel(e)) {
                    return
                }
                setError(true)
            })

        return () => cancel()
    }, [query, pageNumber])

    return { books, hasMore, loading, error }
}
