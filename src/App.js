import React, { useRef, useCallback } from 'react'
import useBookSearch from './useBookSearch'
import { CircularProgress } from '@material-ui/core'

function App() {
    const [query, setQuery] = React.useState('')
    const [pageNumber, setPageNumber] = React.useState(1)
    const { books, hasMore, loading, error } = useBookSearch(query, pageNumber)

    const observer = useRef()
    const lastElement = useCallback(
        (node) => {
            if (loading) return
            if (observer.current) observer.current.disconnect()
            observer.current = new IntersectionObserver((entries) => {
                if (entries[0].isIntersecting && hasMore) {
                    setPageNumber((prev) => prev + 1)
                }
            })
            if (node) observer.current.observe(node)
        },
        [loading]
    )

    const handleSearch = ({ target }) => {
        setQuery(target.value)
        setPageNumber(1)
    }

    return (
        <div style={styles.container}>
            <input
                type="text"
                value={query}
                onChange={handleSearch}
                style={styles.input}
            />
            {books.map((book, index) => {
                if (index === books.length - 1) {
                    return (
                        <div ref={lastElement} key={book}>
                            {book}
                        </div>
                    )
                } else {
                    return <div key={book}>{book}</div>
                }
            })}
            {loading && <CircularProgress />}
            {error && <div style={styles.error}>Error</div>}
        </div>
    )
}

const styles = {
    container: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
    },
    input: {
        fontSize: 20,
        marginBottom: 10,
    },
    error: {
        fontWeight: 600,
        color: '#f00',
    },
}

export default App
